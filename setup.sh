#!/bin/bash

#manager
m1=25.0.0.8
#worker1
w1=25.0.0.11
#worker2
w2=25.0.0.14

source=/opt/source/crawcoviddata

# cd to source code
openssl rand -base64 756 > $source/keyfile.txt
chmod 777 $source/keyfile.txt

#Copy keyfile to two worker1
scp $source/keyfile.txt pdhai@$w1:/opt/secret < "123456"

#Copy keyfile to two worker2
scp $source/keyfile.txt pdhai@$w2:/opt/secret < "123456"

ssh pdhai@$w1 < "123456"

