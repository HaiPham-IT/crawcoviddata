FROM openjdk:17.0.2-oraclelinux8
MAINTAINER haipham
COPY target/crawcoviddata-0.0.1-SNAPSHOT.jar crawcoviddata.jar
ENTRYPOINT [ "java", "-Dspring.profiles.active=prod", "-jar", "/crawcoviddata.jar"]
