rs.status();
db.getSiblingDB("admin").createUser({user: _getEnv('MONGO_INITDB_ROOT_USERNAME'), pwd: _getEnv('MONGO_INITDB_ROOT_PASSWORD'), roles: [ { role: 'root', db: 'admin' } ]});