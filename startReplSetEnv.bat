@echo off

REM SET DELAY=20

REM echo "****** Generate keyfile ******"
REM openssl rand -base64 756 > %cd%\keyfile.txt
REM icacls "%cd%\keyfile.txt" /grant pdhai:F

REM echo "****** Docker compose down for mongodb replSet ******"
REM docker-compose "down"

REM echo "****** Docker compose up for mongodb replSet ******"
REM docker-compose "up" "-d"

REM echo "****** Waiting for %DELAY% seconds for containers to go up ******"

REM timeout "%DELAY%"

docker "exec" "-it" "m1" "/bin/bash" "/scripts/rs-init.sh"