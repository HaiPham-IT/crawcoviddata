package com.crawcoviddata.resources

import com.crawcoviddata.utils.HttpClient
import org.junit.jupiter.api.Test

internal class CoronaTrackerTest {
    private val httpClient = HttpClient()
    private val coronaTracker = CoronaTracker(httpClient)
    @Test
    fun getTotalForAllCountries() {
        val rds = coronaTracker.getTotalForAllCountries()
        assert(rds.isNotEmpty())
    }
}