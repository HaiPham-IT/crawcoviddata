package com.crawcoviddata.service.impl

import com.crawcoviddata.kafka.Producer
import com.crawcoviddata.model.CovidCountry
import com.crawcoviddata.repository.CovidCountryRepo
import com.crawcoviddata.resources.CoronaTracker
import com.crawcoviddata.resources.Diasease
import io.mockk.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.kafka.support.SendResult
import java.util.concurrent.CompletableFuture

internal class CovidCountryServiceImplTest {
    private val covidCountryRepo = mockk<CovidCountryRepo>(relaxed = true)
    private val kafkaTemplate = mockk<KafkaTemplate<String, CovidCountry>>(relaxed = true)
    private val diasease = mockk<Diasease>(relaxed = true)
    private val coronaTracker = mockk<CoronaTracker>(relaxed = true)
    private val producer: Producer = Producer(kafkaTemplate)
    private val service = spyk(CovidCountryServiceImpl(covidCountryRepo, kafkaTemplate, diasease, coronaTracker, producer), recordPrivateCalls = true)

    private val c1 = CovidCountry("1", "Vietnam", "Asia", 10000, 1000, 9000, "https://disease.sh/")
    private val c2 = CovidCountry("2", "Wallis and Futuna", "Australia-Oceania", 761, 7, 438, "https://api.coronatracker.com/")

    @BeforeEach
    fun setup(){}

    @AfterEach
    fun teardown(){
        unmockkAll()
    }
    @Test
    fun `craw data and save the record which is not exist in db`() {
        val sendRs = CompletableFuture<SendResult<String, CovidCountry>>()
        val records = mutableListOf(c1)

        every { diasease.getTotalForAllCountries() } returns records
        every { covidCountryRepo.findOneByCountryAndSource(allAny(), allAny()) } returns null
        every { covidCountryRepo.insert(records[0]) } returns records[0]
        every { kafkaTemplate.send(allAny(), allAny()) } returns sendRs

        service.crawData("Disease")

        verify { diasease.getTotalForAllCountries() }
        every { covidCountryRepo.findOneByCountryAndSource(allAny(), allAny()) }
        verify { covidCountryRepo.insert(records[0]) }
        verify { kafkaTemplate.send(allAny(), allAny()) }
    }

    @Test
    fun `craw data and update the record which is exist in db`(){
        val sendRs = CompletableFuture<SendResult<String, CovidCountry>>()
        val records = mutableListOf(c1)

        every { diasease.getTotalForAllCountries() } returns records
        every { covidCountryRepo.findOneByCountryAndSource(allAny(), allAny()) } returns c2
        every { covidCountryRepo.save(records[0]) } returns records[0]
        every { kafkaTemplate.send(allAny(), allAny()) } returns sendRs

        service.crawData("Disease")

        verify { diasease.getTotalForAllCountries() }
        every { covidCountryRepo.findOneByCountryAndSource(allAny(), allAny()) }
        verify { covidCountryRepo.save(records[0]) }
        verify { kafkaTemplate.send(allAny(), allAny()) }
    }

    @Test
    fun `craw data and not update the record which is exist in db`(){
        val sendRs = CompletableFuture<SendResult<String, CovidCountry>>()
        val records = mutableListOf(c1)

        every { diasease.getTotalForAllCountries() } returns records
        every { covidCountryRepo.findOneByCountryAndSource(allAny(), allAny()) } returns c1
        every { covidCountryRepo.save(records[0]) } returns records[0]
        every { kafkaTemplate.send(allAny(), allAny()) } returns sendRs

        service.crawData("Disease")

        verify { diasease.getTotalForAllCountries() }
        every { covidCountryRepo.findOneByCountryAndSource(allAny(), allAny()) }
        verify (exactly = 0) { covidCountryRepo.save(records[0]) }
        verify (exactly = 0) { kafkaTemplate.send(allAny(), allAny()) }
    }
}