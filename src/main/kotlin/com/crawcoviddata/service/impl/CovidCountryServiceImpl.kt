package com.crawcoviddata.service.impl

import com.crawcoviddata.kafka.Producer
import com.crawcoviddata.model.CovidCountry
import com.crawcoviddata.repository.CovidCountryRepo
import com.crawcoviddata.resources.CoronaTracker
import com.crawcoviddata.resources.Diasease
import com.crawcoviddata.service.CovidCountryService
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service

@Service
class CovidCountryServiceImpl(
    private var covidCountryRepo: CovidCountryRepo,
    private var kafkaTemplate: KafkaTemplate<String, CovidCountry>,
    private var diasease: Diasease = Diasease(),
    private var coronaTracker: CoronaTracker = CoronaTracker(),
    private var producer: Producer = Producer(kafkaTemplate)
): CovidCountryService {
    override fun crawData(resource: String) {
        val data: List<CovidCountry>? = when(resource){
            "Disease" -> diasease.getTotalForAllCountries()
            "Coronatracker" -> coronaTracker.getTotalForAllCountries()
            else -> null
        }
        data?.forEach { d: CovidCountry -> handleRecord(d) }
    }

    private fun handleRecord(covidCountry: CovidCountry){
        when(val oldRd = covidCountryRepo.findOneByCountryAndSource(covidCountry.country, covidCountry.source)) {
            null -> {
                val newRc = covidCountryRepo.insert(covidCountry)
                producer.sendMessage("covidcountry", newRc)
            }
            else -> {
                if(isChange(oldRd, covidCountry)) {
                    covidCountry.also { it.id  = oldRd.id}
                    val newRc = covidCountryRepo.save(covidCountry)
                    producer.sendMessage("covidcountry", newRc)
                }
            }
        }
    }

    private fun isChange(oldRd: CovidCountry, newRd: CovidCountry): Boolean {
        return !oldRd.continent.equals(newRd.continent, true) || oldRd.cases != newRd.cases
                || oldRd.deaths != newRd.deaths || oldRd.recovered != newRd.recovered
    }
}