package com.crawcoviddata.resources

import com.crawcoviddata.dto.DiseaseCovidCountry
import com.crawcoviddata.model.CovidCountry
import com.crawcoviddata.utils.HttpClient
import com.google.gson.Gson

class Diasease(
    private val httpClient: HttpClient = HttpClient()
){
    private val endpoint: String = "https://disease.sh/"
    private val total4allcountries: String = "v3/covid-19/countries/"

    fun getTotalForAllCountries(): List<CovidCountry> {
        val client = httpClient.createHttpClient()
        val request = httpClient.createRequest(endpoint, total4allcountries, null)
        val response = httpClient.sendRequest(client, request)
        val diseaseTotalAllCountries = Gson().fromJson(response.body(), Array<DiseaseCovidCountry>::class.java).toList()
        return diseaseTotalAllCountries.map { it.toCovidCountry() }
    }
}
