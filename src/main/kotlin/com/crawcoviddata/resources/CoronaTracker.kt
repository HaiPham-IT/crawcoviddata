package com.crawcoviddata.resources

import com.crawcoviddata.dto.TrackerCovidCountry
import com.crawcoviddata.model.CovidCountry
import com.crawcoviddata.utils.HttpClient
import com.google.gson.Gson

class CoronaTracker(
    private val httpClient: HttpClient = HttpClient()
){
    private val endpoint: String = "https://api.coronatracker.com/"
    private val total4allcountries: String = "v3/stats/worldometer/country"

    fun getTotalForAllCountries(): List<CovidCountry> {
        val client = httpClient.createHttpClient()
        val request = httpClient.createRequest(endpoint, total4allcountries, null)
        val response = httpClient.sendRequest(client, request)
        val trackerCovidCountries = Gson().fromJson(response.body(), Array<TrackerCovidCountry>::class.java).toList()
        return trackerCovidCountries.map { it.toCovidCountry() }
    }
}