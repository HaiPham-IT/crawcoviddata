package com.crawcoviddata.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document("CovidCountries")
data class CovidCountry (
    @Id
    var id: String? = null,
    var country: String?,
    var continent: String?,
    var cases: Int?,
    var deaths: Int?,
    var recovered: Int?,
    var source: String?
){}