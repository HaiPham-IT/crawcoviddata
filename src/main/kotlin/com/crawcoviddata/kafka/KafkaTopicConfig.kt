package com.crawcoviddata.kafka

import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.admin.NewTopic
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.kafka.config.TopicBuilder
import org.springframework.kafka.core.KafkaAdmin

@Configuration
class KafkaTopicConfig (
    @Value("\${spring.kafka.bootstrap-server}")
    private var bootstrapServer: String
        ){
    @Bean
    fun admin(): KafkaAdmin = KafkaAdmin(mapOf(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG to bootstrapServer))
    @Bean
    fun covidCountryTopic(): NewTopic{
        return TopicBuilder.name("covidcountry")
            .replicas(2).partitions(3).build()
    }
}