package com.crawcoviddata.kafka

import com.crawcoviddata.model.CovidCountry
import org.springframework.kafka.core.KafkaTemplate

class Producer(private val kafkaTemplate: KafkaTemplate<String, CovidCountry>) {

    fun sendMessage(topic: String, message: CovidCountry ){
        kafkaTemplate.send(topic, message)
    }
}