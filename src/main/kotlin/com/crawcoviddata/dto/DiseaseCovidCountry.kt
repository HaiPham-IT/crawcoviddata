package com.crawcoviddata.dto

import com.crawcoviddata.model.CovidCountry

data class DiseaseCovidCountry(
    private var updated: Long?,
    private var country: String?,
    private var countryInfo: CountryInfo?,
    private var cases: Int?,
    private var todayCases: Int?,
    private var deaths: Int?,
    private var todayDeaths: Int?,
    private var recovered: Int?,
    private var todayRecovered: Int?,
    private var active: Int?,
    private var critical: Int?,
    private var casesPerOneMillion: Int?,
    private var deathsPerOneMillion: Int?,
    private var tests: Int?,
    private var testsPerOneMillion: Int?,
    private var population: Int?,
    private var continent: String?,
    private var oneCasePerPeople: Int?,
    private var oneDeathPerPeople: Int?,
    private var oneTestPerPeople: Int?,
    private var activePerOneMillion: Double?,
    private var recoveredPerOneMillion: Double?,
    private var criticalPerOneMillion: Double?
): CovidCountryable {
    override fun toCovidCountry(): CovidCountry {
        return CovidCountry(null, this.country, this.continent, this.cases, this.deaths,
            this.recovered, "https://disease.sh/")
    }
}