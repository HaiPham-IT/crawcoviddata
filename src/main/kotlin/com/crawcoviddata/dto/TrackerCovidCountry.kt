package com.crawcoviddata.dto

import com.crawcoviddata.model.CovidCountry

data class TrackerCovidCountry(
    private var countryCode: String?,
    private var country: String?,
    private var countryName: String?,
    private var lat: Double?,
    private var lng: Double?,
    private var totalConfirmed: Int?,
    private var totalDeaths: Int?,
    private var totalRecovered: Int?,
    private var dailyConfirmed: Int?,
    private var dailyDeaths: Int?,
    private var activeCases: Int?,
    private var totalCritical: Int?,
    private var totalConfirmedPerMillionPopulation: Int?,
    private var totalDeathsPerMillionPopulation: Int?,
    private var FR: Double?,
    private var PR: Double?,
    private var lastUpdated: String?
): CovidCountryable{
    override fun toCovidCountry(): CovidCountry {
        return CovidCountry(null, this.country, null, this.totalConfirmed, this.totalDeaths, this.totalRecovered, "https://api.coronatracker.com/")
    }
}