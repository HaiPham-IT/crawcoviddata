package com.crawcoviddata.dto

import com.crawcoviddata.model.CovidCountry

interface CovidCountryable {
    fun toCovidCountry(): CovidCountry
}