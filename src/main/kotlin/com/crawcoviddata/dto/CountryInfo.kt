package com.crawcoviddata.dto

data class CountryInfo(
    private var _id: Int?,
    private var iso2: String?,
    private var iso3: String?,
    private var lat: Double?,
    private var long: Double?,
    private var flag: String?
) {}