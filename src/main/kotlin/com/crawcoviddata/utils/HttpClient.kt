package com.crawcoviddata.utils

import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

class HttpClient{
    fun createParam(params: Map<String, String>): String = params.map { (k, v) -> "${k}=${v}" }.joinToString { "&" }
    fun createHttpClient(): HttpClient = HttpClient.newBuilder().build()
    fun createRequest(endPoint: String, route: String, urlParams: String?): HttpRequest {
        println("End_point: $endPoint")
        println("Route: $route")
        println("Params: $urlParams")
        return HttpRequest.newBuilder().uri(URI.create("$endPoint$route?${if(urlParams.isNullOrEmpty()) String() else urlParams }")).build()
    }
    fun sendRequest(client: HttpClient, request: HttpRequest): HttpResponse<String> {
        println("Send request.")
        return client.send(request, HttpResponse.BodyHandlers.ofString())
    }
}