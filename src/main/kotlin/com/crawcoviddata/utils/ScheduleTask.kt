package com.crawcoviddata.utils

import com.crawcoviddata.service.impl.CovidCountryServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class ScheduleTask {
    @Autowired
    private lateinit var covidCountryService: CovidCountryServiceImpl

    @Scheduled(fixedDelay = 1000 * 60 * 60 * 3)
    fun getCovid19Data(){
        val resource = listOf("Disease")
        resource.map{ covidCountryService.crawData(it) }
    }
}