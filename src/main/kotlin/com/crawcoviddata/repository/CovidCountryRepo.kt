package com.crawcoviddata.repository

import com.crawcoviddata.model.CovidCountry
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query

interface CovidCountryRepo: MongoRepository<CovidCountry, String> {
    @Query("{country: '?0', source: '?1'}")
    fun findOneByCountryAndSource(country: String?, source: String?): CovidCountry?
}