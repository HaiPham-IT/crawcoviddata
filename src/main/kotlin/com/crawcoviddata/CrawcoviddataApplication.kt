package com.crawcoviddata

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class CrawcoviddataApplication

fun main(args: Array<String>) {
	runApplication<CrawcoviddataApplication>(*args)
}