#!/bin/bash

DELAY=25

mongo < /scripts/config.js

echo "****** Waiting for ${DELAY} seconds for replicaset configuration to be applied ******"

sleep $DELAY

mongo < /scripts/init.js

mongo -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD --authenticationDatabase admin <<EOF
db.adminCommand({"setDefaultRWConcern" : 1,"defaultWriteConcern" : {"w":2}});
rs.addArb("25.0.0.11:27017");
EOF

echo "****** Waiting for ${DELAY} seconds for abiter to be added to replicaset ******"

sleep $DELAY

